import 'package:flutter/material.dart';

class SimpleButton extends StatefulWidget {
  final Icon iconData;
  final void Function() onPressed;
  const SimpleButton({
    required this.iconData,
    required this.onPressed,
    Key? key}) : super(key: key);

  @override
  _SimpleButtonState createState() => _SimpleButtonState();
}

class _SimpleButtonState extends State<SimpleButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: widget.iconData,
      onPressed: widget.onPressed,
    );
  }
}
